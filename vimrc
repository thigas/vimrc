" github: https://github.com/thiagocezarns/vimrc

"------------------------------------------------------------
"               PLUGINS 
"------------------------------------------------------------

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'gruvbox-community/gruvbox'
Plug 'https://github.com/ctrlpvim/ctrlp.vim.git'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'
Plug 'marcweber/vim-addon-mw-utils'
"Plug 'garbas/vim-snipmate'
Plug 'tpope/vim-vividchalk'

call plug#end()

"let g:snipMate = { 'snippet_version' : 1 } 

"------------------------------------------------------------

set nocompatible
filetype indent plugin on
syntax on

"------------------------------------------------------------
" Must have options {{{1

set hidden
set wildmenu
set showcmd
set hlsearch
set noswapfile
set incsearch
set scrolloff=8
set colorcolumn=100

"------------------------------------------------------------
" Usability options {{{1

set ignorecase
set smartcase
set backspace=indent,eol,start
set autoindent
set nostartofline
set ruler
set laststatus=2
set confirm
set visualbell
set t_vb=
set cmdheight=2
set number
set notimeout ttimeout ttimeoutlen=200
" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>


"------------------------------------------------------------
"               INDENTATION OPTIONS
"------------------------------------------------------------

set shiftwidth=4
set softtabstop=4
set expandtab


"------------------------------------------------------------
"               MAPPINGS 
"------------------------------------------------------------

map Y y$
nnoremap <C-L> :nohl<CR><C-L>


"------------------------------------------------------------
"               SEARCHING
"------------------------------------------------------------

" Ctrl-P Options
"Don't forget to install fd
if executable('fd')
    let g:ctrlp_user_command = 'fd --type f --color never "" %s'
endif
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_use_caching = 0

"------------------------------------------------------------
"               COLORS
"------------------------------------------------------------

"let g:gruvbox_contrast_dark = 'hard'
"colorscheme gruvbox
colorscheme vividchalk
